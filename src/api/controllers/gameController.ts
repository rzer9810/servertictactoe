import { Server, Socket } from 'socket.io';
import { ConnectedSocket, MessageBody, OnMessage, SocketController, SocketIO } from "socket-controllers";

// controlador para actualizar el juego en tiempo real
@SocketController()
export class gameController {

    // tomar el sockect id de la sala
    private getSocketGameRoom(socket: Socket): string {

        const socketRooms = Array.from(socket.rooms.values()).filter(
            (r) => r !== socket.id
        );

        const gameRoom = socketRooms && socketRooms[0];

        return gameRoom;
    }

    // Actualizar juego
    @OnMessage("update_game")
    public async updateGame(
        @SocketIO() io: Server,
        @ConnectedSocket() socket: Socket,
        @MessageBody() message: any
    ) {
        const gameRoom = this.getSocketGameRoom(socket);
        socket.to(gameRoom).emit("on_game_update", message);
    }

    
    // Ganador juego
    @OnMessage("game_win")
    public async gameWin(
        @SocketIO() io: Server,
        @ConnectedSocket() socket: Socket,
        @MessageBody() message: any
    ) {
        const gameRoom = this.getSocketGameRoom(socket);
        socket.to(gameRoom).emit("on_game_win", message);
    }

}
