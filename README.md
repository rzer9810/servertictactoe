### `npm install`
Instala todas las dependencias antes de iniciar la app

### `npm start`
Ejecuta la aplicación en el modo de desarrollo.\
Abra [http://localhost:9000](http://localhost:9000) para verlo en el navegador.

# librerias usadas
socket.io
reflect-metadata
socket-controllers
express

# Funcionamiento
1. recuerda instalar las dependencias y librerias con el comando "npm i"
2. Inicia la app con el comando "npm start"